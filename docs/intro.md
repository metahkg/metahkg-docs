---
sidebar_position: 1
---
# Metahkg

See the [metahkg repository](https://gitlab.com/metahkg/metahkg).

## Getting Started

Get started by trying a [deployment](/docs/category/deploy-metahkg).

### What you'll need

See [requirements](./deploy/setup/requirements) for more information.

## Api docs

See [here](/docs/category/api) for the api docs.

## Metahkg Api Client

See [gitlab.com/metahkg/metahkg-api](https://gitlab.com/metahkg/metahkg-api) for the typescript api client.

