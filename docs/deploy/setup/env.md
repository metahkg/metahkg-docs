---
sidebar_position: 4
---

# Environmental variables

:::tip
This can be automated by running:
```bash
./setup.sh -c
```
:::

## Copy environment variables

```bash
cd docker
cp example.env .env
```

## Edit .env

See your docker/.env file.