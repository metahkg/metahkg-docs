---
sidebar_position: 3
---

# Using script

This script will install dependencies and help you set up configuration for metahkg.
You are recommened to read the script before running.

The script works on the latest versions of the supported oses listed in [requirements](./requirements.md).

:::danger
Never run the script as root!
Run as a normal user with sudo privilege.
:::

```bash
./setup.sh
```
